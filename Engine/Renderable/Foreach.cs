﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Engine
{
    class Foreach : ScopableRenderable
    {
        public static string pattern = String.Format(@"#foreach\((?:{0})\s+in\s+(?:{1})\)", Variable.variableNamePattern, Variable.pattern);
        public static Regex regex = new Regex("^" + pattern + "$");
        private string textInside;
        private string mVariableName;
        private string[] mConteinerPath;
        public Foreach(Fragment pFragment)
            :base(pFragment)
        {
        }
        public override void processFragment(Fragment pFragment)
        {
            textInside = pFragment.TextInside;
            int indexOfFirstBracket = textInside.IndexOf("(");
            string insideBrackets = textInside.Substring(indexOfFirstBracket + 1, textInside.Length - indexOfFirstBracket - 2);
            mVariableName = insideBrackets.Substring(1, insideBrackets.IndexOf(" ") - 1);
            string conteiner = insideBrackets.Substring(insideBrackets.LastIndexOf("$") + 1);
            mConteinerPath = conteiner.Split('.');
        }
        public override string render(ref Context pContext, ref MacroTable pMacroTable)
        {
            JArray iterable = pContext.getConteiner(mConteinerPath);
            if (iterable != null)
            {             
                string parsedOutput = "";
                foreach (JToken lData in iterable)
                {
                    pContext.addVariable(mVariableName, lData);
                    string iterationResult = "";
                    try
                    {
                        foreach (Renderable child in mChildren)
                        {
                            iterationResult += child.render(ref pContext, ref pMacroTable);
                        }
                    }
                    catch(BreakException)
                    {
                        parsedOutput += iterationResult;
                        pContext.removeVariable(mVariableName);
                        return parsedOutput;
                    }
                    parsedOutput += iterationResult;
                }
                pContext.removeVariable(mVariableName);
                return parsedOutput;
            }
            else
            {
                return textInside;
            }
        }
    }
}
