﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine
{
    class Root : Renderable
    {
        public Root(Fragment pFragment = null) :
            base(pFragment)
        {

        }
        public override string render(ref Context pContext, ref MacroTable pMacroTable)
        {
            string result = "";
            foreach (Renderable child in mChildren)
            {
                result += child.render(ref pContext, ref pMacroTable);
            }
            return result;
        }
    }
}
