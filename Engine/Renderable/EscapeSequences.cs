﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Engine
{
    class EscapeSequences : Renderable
    {
        public static string pattern = @"\\.";
        public static Regex regex = new Regex("^" + pattern + "$");

        private string letter = "";
        public EscapeSequences(Fragment pFragment)
            :base(pFragment)
        {
        }
        public override void processFragment(Fragment pFragment)
        {
            letter = pFragment.TextInside.Substring(1);
        }
        public override string render(ref Context pContext, ref MacroTable pMacroTable)
        {
            return letter;
        }
    }
}
