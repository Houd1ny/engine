﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Engine
{
    class Break : Renderable
    {
        public static string pattern = @"#break";
        public static Regex regex = new Regex("^" + pattern + "$");
        public Break(Fragment pFragment)
            :base(pFragment)
        {
        }
        public override string render(ref Context pContext, ref MacroTable pMacroTable)
        {
            Renderable renderableParent = this.parent;
            while (renderableParent != null)
            {
                if (renderableParent.GetType() == typeof(Foreach))
                {
                    throw new BreakException();
                }
                else
                {
                    renderableParent = renderableParent.parent;
                }
            }
            Log.Error("breck outside foreach");
            return "";
        }
    }
    class BreakException : Exception
    {
    }
}
