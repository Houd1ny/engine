﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine
{
    class PlainText : Renderable
    {
        private string mTextInside;
        public PlainText(Fragment pFragment) :
            base(pFragment)
        {

        }
        public override void processFragment(Fragment pFragment)
        {
            mTextInside = pFragment.TextInside;
        }
        public override string render(ref Context pContext, ref MacroTable pMacroTable)
        {
            return mTextInside;
        }
    }
}
