﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Engine
{
    class Variable : Renderable
    {
        //TODO fail if variable end with .
        public static string pattern = @"\$[a-zA-Z][a-zA-Z0-9_.]*";
        public static string variableNamePattern = @"\$[a-zA-Z][a-zA-Z0-9_]*";
        public static Regex regex = new Regex("^" + pattern + "$");
        private string textInside;
        private string[] callList;
        public Variable(Fragment pFragment)
            : base(pFragment)
        {
        }
        public static object evalVariable(string pVariableString, Context pContext)
        {
            if (!Variable.regex.IsMatch(pVariableString)) return null;
            string variableText = pVariableString.Substring(1);
            string[] callList = variableText.Split('.');
            return pContext.getVariable(callList);
        }
        public override void processFragment(Fragment pFragment)
        {
            textInside = pFragment.TextInside;
            string variableText = pFragment.TextInside.Substring(1);
            callList = variableText.Split('.');
        }
        public override string render(ref Context pContext, ref MacroTable pMacroTable)
        {
            object variable = pContext.getVariable(callList);
            if (variable != null)
            {
                return variable.ToString();
            }
            else
            {
                return textInside;
            }
        }
    }
}
