﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Engine
{
    class ElseIf : Renderable
    {
        public static string pattern = @"#elseif\((?:.*)\)";
        public static Regex regex = new Regex("^" + pattern + "$");
        private Fragment mFragment;
        public ElseIf(Fragment pFragment)
            :base(pFragment)
        {
            mFragment = pFragment;
        }
        public override string render(ref Context pContext, ref MacroTable pMacroTable)
        {
            return "";
        }
        public Fragment getFragment()
        {
            return mFragment;
        }
    }
}
