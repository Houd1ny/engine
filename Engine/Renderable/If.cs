﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using ExpressionEvaluator;

namespace Engine
{
    class If : ScopableRenderable
    {
        public static string pattern = @"#if\s*\((?:.*)\)";
        public static Regex regex = new Regex("^" + pattern + "$");
     
        private string textInside;
        private Renderable leftBranch = null;
        private Renderable rightBranch = null;
        private string expresionInside = "";
        private bool finishedLeftBranch = false;

        public If(Fragment pFragment)
            :base(pFragment)
        {
            leftBranch = new Root();
            leftBranch.parent = this;
            rightBranch = null;
        }
        public override void processFragment(Fragment pFragment)
        {
            textInside = pFragment.TextInside;
            int indexOfFirstBracket = textInside.IndexOf("(");
            expresionInside = textInside.Substring(indexOfFirstBracket + 1, textInside.Length - indexOfFirstBracket - 2);
        }
        public override void addChild(Renderable pRenderable)
        {
            base.addChild(pRenderable);
            if(!finishedLeftBranch)
            {
                if (pRenderable.GetType() == typeof(Else))
                {
                    rightBranch = new Root();
                    rightBranch.parent = this;
                    finishedLeftBranch = true;
                }
                else if (pRenderable.GetType() == typeof(ElseIf))
                {
                    rightBranch = new If(((ElseIf)pRenderable).getFragment());
                    rightBranch.parent = this;
                    finishedLeftBranch = true;
                }
                else
                {
                    leftBranch.addChild(pRenderable);
                }
            }
            else
            {
                rightBranch.addChild(pRenderable);
            }
            
        }
        public override string render(ref Context pContext, ref MacroTable pMacroTable)
        {
            //preparing expression
            string compiledExpresion = "";
            //expresionInside = expresionInside.Replace(" ", "");
            string[] splited = new Regex(String.Format(@"({0})", Variable.pattern)).Split(expresionInside);        
            foreach (string piece in splited)
            {
                if (Variable.regex.IsMatch(piece))
                {
                    string[] varibleCallList = piece.Substring(1).Split('.');
                    object variable = pContext.getVariable(varibleCallList);
                    if (variable != null )
                    {
                        if (variable.GetType() == typeof(String))
                        {
                            compiledExpresion += ("\'" + variable.ToString() + "\'");
                        }
                        else
                        {
                            compiledExpresion += variable.ToString();
                        }
                    }
                }
                else
                {
                    Regex regExp = new Regex("(\\\\\")");
                    foreach(string a in regExp.Split(piece))
                    {
                        if (regExp.IsMatch(a))
                        {
                            compiledExpresion += a.Replace("\\\"", "\"");
                        }
                        else
                        {
                            compiledExpresion += a.Replace("\"", "'"); ;
                        }
                    }
                    //string a = regExp.Replace(piece, "");
                    //string parsed = piece.Replace("\"", "'");
                }
            }
            //https://csharpeval.codeplex.com/
            var expressionEvaluator = new CompiledExpression(compiledExpresion);
            object resultOfEval;
            try
            {
                resultOfEval = expressionEvaluator.Eval();
            }
            catch (ExpressionEvaluator.ParseException e)
            {
                Log.Error("Can`t parse expresion  " + compiledExpresion + " variable  " + expresionInside + "\n" + e.InnerException.Message);
                if (rightBranch != null)
                {
                    return rightBranch.render(ref pContext, ref pMacroTable);
                }
                else
                {
                    return "";
                }
            }
            try
            {
                if (Convert.ToBoolean(resultOfEval))
                {
                    return leftBranch.render(ref pContext, ref pMacroTable);
                }
                else
                {
                    return rightBranch.render(ref pContext, ref pMacroTable);
                } 
            }
            catch(System.FormatException)
            {
                if (resultOfEval.GetType() == typeof(string))
                {
                    string insideIf = (string)resultOfEval;
                    if (insideIf.Length != 0)
                    {
                        return leftBranch.render(ref pContext, ref pMacroTable);
                    }
                    else
                    {
                        return rightBranch.render(ref pContext, ref pMacroTable);
                    } 
                }
                Log.Error("System.Convertor don`t know how to convert " + resultOfEval + " type " + resultOfEval.GetType());
                if (rightBranch != null)
                {
                    return rightBranch.render(ref pContext, ref pMacroTable);
                }
                else
                {
                    return "";
                }
            }
        }
    }
}
