﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Engine
{
    class MacroDefine : ScopableRenderable
    {
        public static string macroNamePattern = @"[A-Za-z][A-Za-z0-9_]*";
        public static string pattern = String.Format(@"#macro\(\s*(?:{0})(?:\s*|(?:\s+(?:{1})\s*)+)\)", macroNamePattern, Variable.pattern);
        public static Regex regex = new Regex("^" + pattern + "$");

        private string mMacroName;
        private string[] mArgsList;
        public int CountOfArgs
        {
            get
            {
                return mArgsList.Length;
            }
        }

        public MacroDefine(Fragment pFragment)
            :base(pFragment)
        {
        }
        public override void processFragment(Fragment pFragment)
        {
            int indexFirstBracket = pFragment.TextInside.IndexOf('(');
            string insideBrackets = pFragment.TextInside.Substring(indexFirstBracket + 1, pFragment.TextInside.Length - indexFirstBracket - 2);
            insideBrackets = insideBrackets.Replace(" ", "");
            string[] args = insideBrackets.Split('$');
            mMacroName = args[0];
            mArgsList = new string[args.Length - 1];
            for (int i = 1; i < args.Length; ++i)
            {
                mArgsList[i - 1] = args[i];
            }
        }
        public override string render(ref Context pContext, ref MacroTable pMacroTable)
        {
            pMacroTable.addMacro(mMacroName, this);
            return "";
        }
        public string renderWithArgs(ref Context pContext, ref MacroTable pMacroTable)
        {
            string result = "";
            foreach (Renderable child in mChildren)
            {
                result += child.render(ref pContext, ref pMacroTable);
            }
            return result;
        }
        public string[] getArgsList()
        {
            return mArgsList;
        }
    }
}
