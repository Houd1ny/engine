﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Engine
{
    class Set : Renderable
    {
        public static string boolPattern = @"(?:false|true)";
        public static string stringPattern = @"""[^""\r\n\\]*(?:\\.[^""\r\n\\]*)*""";
        public static string allDigitsPattern = @"[-+]?[0-9]*\.?[0-9]+";
        public static string literalPattern = string.Format("(?:{0}|{1}|{2})", boolPattern, stringPattern, allDigitsPattern);
        public static string pattern = String.Format(@"#set\(\s*{0}\s*=\s*(?:{1}|{2})\s*\)", Variable.variableNamePattern, literalPattern, Variable.pattern);
        public static Regex regexToGetVariables = new Regex(String.Format(@"^#set\(\s*({0})\s*=\s*({1}|{2})\s*\)$", Variable.variableNamePattern, literalPattern, Variable.pattern));
        public static Regex regex = new Regex("^" + pattern + "$");
        
        private string variableName;
        private string token;
        
        public Set(Fragment pFragment)
            :base(pFragment)
        {
        }
        public override void processFragment(Fragment pFragment)
        {
            Match match = regexToGetVariables.Match(pFragment.TextInside);
            variableName = match.Groups[1].ToString().Substring(1);
            token = match.Groups[2].ToString();
        }
        public override string render(ref Context pContext, ref MacroTable pMacroTable)
        {
            JToken result = null;
            Regex literalRegexp = new Regex("^" + Set.literalPattern + "$");
            if (Variable.regex.IsMatch(token))
            {
                result = pContext.getJToken(token.Substring(1).Split('.'));
            }
            else if (literalRegexp.IsMatch(token))
            {
                result = JsonConvert.DeserializeObject<JToken>(token);
            }
            pContext.addVariable(variableName, result);
            return "";
        }
    }
}
