﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Engine
{
    class Else : Renderable
    {
        public static string pattern = @"#else";
        public static Regex regex = new Regex("^" + pattern + "$");
        public Else(Fragment pFragment)
            :base(pFragment)
        {
        }
        public override string render(ref Context pContext, ref MacroTable pMacroTable)
        {
            return "";
        }
    }
}
