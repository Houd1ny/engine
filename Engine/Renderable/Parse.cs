﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.IO;

namespace Engine
{
    class Parse : Renderable
    {
        public static string pattern = "#parse\\(\\s*\".*?\"\\s*\\)";
        public static string patternToFindVariable = "#parse\\(\\s*\"(.*?)\"\\s*\\)";
        public static Regex regex = new Regex("^" + pattern + "$");

        private string filePath = "";
        public Parse(Fragment pFragment)
            :base(pFragment)
        {

        }
        public override void processFragment(Fragment pFragment)
        {
            Match match = new Regex(patternToFindVariable).Match(pFragment.TextInside);
            filePath = match.Groups[1].ToString();
        }
        public override string render(ref Context pContext, ref MacroTable pMacroTable)
        {
            string importFileText = "";
            try
            {
                importFileText = File.ReadAllText(filePath);
            }
            catch(Exception)
            {
                Log.Error("Include::render cannot render file with path " + filePath);
            }
            Compiler compiler = new Compiler(importFileText);
            Renderable fileRoot = compiler.compile();
            return fileRoot.render(ref pContext, ref pMacroTable);
        }
    }
}
