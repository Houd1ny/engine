﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine
{
    abstract class Renderable
    {
        protected List<Renderable> mChildren;
        public bool createsScope { get; protected set; }
        public Renderable parent = null;
        public Renderable(Fragment pFragment)
        {
            mChildren = new List<Renderable>();
            createsScope = false;
            processFragment(pFragment);
        }
        public virtual void addChild(Renderable pRenderable)
        {
            mChildren.Add(pRenderable);
        }
        virtual public void processFragment(Fragment pFragment)
        {

        }
        virtual public void enterScope()
        {

        }
        virtual public void exitScope()
        {

        }
        abstract public string render(ref Context pContext, ref MacroTable pMacroTable);    
    }
}
