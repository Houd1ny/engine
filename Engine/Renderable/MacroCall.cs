﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Engine
{
    class MacroCall : Renderable
    {
        public static string pattern = String.Format(@"#{0}\(\s*(?:\s*(?:{1}|{2}|{3})\s*)*\s*\)", MacroDefine.macroNamePattern, Variable.pattern, Set.literalPattern, Variable.pattern);
        public static Regex regex = new Regex("^" + pattern + "$");

        private string mMacroName;
        private List<string> mArgsList;

        public MacroCall(Fragment pFragment)
            :base(pFragment)
        {
        }
        public override void processFragment(Fragment pFragment)
        {
            int indexFirstBracket = pFragment.TextInside.IndexOf('(');
            mMacroName = pFragment.TextInside.Substring(1, indexFirstBracket - 1);
            string insideBrackets = pFragment.TextInside.Substring(indexFirstBracket + 1, pFragment.TextInside.Length - indexFirstBracket - 2);
            string pattern = String.Format("({0}|{1})",
                    Set.literalPattern,
                    Variable.pattern
                    );  
            //Match match = new Regex(pattern).Split(insideBrackets);
            mArgsList = new List<string>();
            Regex literalRegexp = new Regex("^" + Set.literalPattern +  "$");
            foreach (string a in new Regex(pattern).Split(insideBrackets))
            {
                if (Variable.regex.IsMatch(a))
                {
                    mArgsList.Add(a);
                }
                else if (literalRegexp.IsMatch(a))
                {
                    mArgsList.Add(a);
                }              
            }
        }
        public override string render(ref Context pContext, ref MacroTable pMacroTable)
        {
            Regex literalRegexp = new Regex("^" + Set.literalPattern + "$");
            if (pMacroTable.ContainsKey(mMacroName))
            {
                MacroDefine macros = pMacroTable.getMacro(mMacroName);
                if (mArgsList.Count == macros.CountOfArgs)
                {
                    for (int i = 0; i < mArgsList.Count; ++i)
                    {
                        string variable = mArgsList[i];
                        if (Variable.regex.IsMatch(variable))
                        {
                            JToken token = pContext.getJToken(mArgsList[i].Split('.'));
                            pContext.addVariable(macros.getArgsList()[i], token);
                        }
                        else if (literalRegexp.IsMatch(variable))
                        {
                            try
                            {
                                JToken jsonToken = JsonConvert.DeserializeObject<JToken>(variable);
                                pContext.addVariable(macros.getArgsList()[i], jsonToken);
                            }
                            catch(Exception e)
                            {
                                Log.Error(string.Format("Can`t put variable in context{0}\nError {1}", variable, e.Message));
                            }
                        }                       
                    }
                    string resultOfParsing = macros.renderWithArgs(ref pContext, ref pMacroTable);
                    for (int i = 0; i < mArgsList.Count; ++i)
                    {
                        pContext.removeVariable(macros.getArgsList()[i]);
                    }
                    return resultOfParsing;
                }
                else
                {
                    Log.Error(String.Format("MacroCall::render You trying to call macro {0} with dif count of args", mMacroName));
                }
                return "";
            }
            else
            {
                Log.Error(String.Format("MacroCall::render {0} macros is not exist", mMacroName));
                return "";
            }           
        }
    }
}
