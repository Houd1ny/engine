﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.IO;
using System.Text.RegularExpressions;

namespace Engine
{
    class Config
    {
        public string mRootPath { get; set; }
        public string[] mPreloadedFiles { get; set; }
        public Dictionary<string, Context> mFilesToParse{ get; set; }
        public Dictionary<string, string> mInputToOutput { get; set; }
        public Config(string path)
        {
            mFilesToParse = new Dictionary<string, Context>();
            mInputToOutput = new Dictionary<string, string>();
            string jsonString = "";
            try
            {
                jsonString = File.ReadAllText(path);
            }
            catch(Exception e)
            {
                Log.Error("Cannot open json config file\nError " + e.Message);
            }
            JObject jsonConfig = null;
            try
            {
                jsonConfig = JsonConvert.DeserializeObject<JObject>(jsonString);
            }
            catch (Exception e)
            {
                Log.Error("Cannot parse json file\nError " + e.Message);
            }
            string mConfigFileText = "";
            try
            {
                string conFonfigPath = (string)jsonConfig.GetValue("configFile");
                mConfigFileText = File.ReadAllText(conFonfigPath);
            }
            catch (Exception e)
            {
                Log.Error("Cannot find property configFile in config file or read file\nError " + e.Message);
            }
            Match rootMatch= new Regex("root=(.*?)\\s*\n").Match(mConfigFileText);
            if (rootMatch.Success)
            {
                mRootPath = rootMatch.Groups[1].ToString();
            }
            int indexOfPreloaded = mConfigFileText.IndexOf("preloaded=");
            string preloaded = mConfigFileText.Substring(indexOfPreloaded + "preloaded=".Length);
            mPreloadedFiles = preloaded.Split(',');
            JToken[] array = null;
            try
            {
                array = jsonConfig["pages"].ToArray();
            }
            catch (Exception e)
            {
                Log.Error("Cannot find property pages or it`s not an array\nError " + e.Message);
                return;
            }
            foreach (JObject item in array)
            {
                try
                {
                    string fileName = (string)item["page"];
                    item.Remove("page");

                    string outputFilePath = (string)item["outputPath"];
                    item.Remove("outputPath");
                    mInputToOutput.Add(fileName, outputFilePath);
                    foreach (JObject param in item["params"].ToArray())
                    {
                        item.Add((string)param["key"], param["value"]);
                    }
                    Context contex = new Context(item);
                    mFilesToParse.Add(fileName, contex);
                }
                catch (Exception e)
                {
                    Log.Error("Cannot read page in config\nError " + e.Message);
                }
            }
        }
    }
}
