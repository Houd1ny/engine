﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.IO;
using System.Text.RegularExpressions;

namespace Engine
{
    class Program
    {
        static void Main(string[] args)
        {
            Log.ConsoleOutputEnabled = false;
            Log.FileOutputEnabled = true;
            Log.InfoLogEnabled = true;
            Log.ErrorLogEnabled = true;
            if (args.Length < 1)
            {
                Log.Error("No config file");
                return;
            }
            Config config = new Config(args[0]);
            MacroEngine macroEngine = new MacroEngine();
            macroEngine.merge(config);
            Console.ReadKey();
        }
    }
}
