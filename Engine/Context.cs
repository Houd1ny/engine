﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Engine
{
    class Context : ICloneable
    {
        //http://stackoverflow.com/questions/9662699/parse-encode-json-without-deserializing-serializing-classes
        //http://www.newtonsoft.com/json/help/html/T_Newtonsoft_Json_Linq_JObject.htm
        //http://www.newtonsoft.com/json/help/html/T_Newtonsoft_Json_Linq_JToken.htm
        public JObject mDada {get; private set;}
        public Context(JObject pJObject):
            base()
        {
            mDada = pJObject;
        }
        public Context():
            base()
        {
            mDada = JsonConvert.DeserializeObject<JObject>("{}");
        }
        public JToken getJToken(string[] pCallList)
        {
            JToken rValue = null;
            JObject root = mDada;
            foreach (string nameOfProperty in pCallList)
            {
                if (root != null && root.TryGetValue(nameOfProperty, out rValue))
                {
                    root = rValue as JObject;
                }
                else
                {
                    Log.Error(String.Format("Contex.getJToken ::: \"{0}\" this name do not exist in context \n property ${1}", nameOfProperty, String.Join(".", pCallList)));
                    return null;
                }
            }
            return rValue;
        }
        public object getVariable(string[] pCallList)
        {
            JToken lJToken = getJToken(pCallList);
            if (lJToken != null)
            {
                JTokenType typeOfVariable = lJToken.Type;
                object rObject = null;
                switch (typeOfVariable)
                {
                   /*case JTokenType.Array:
                        Log.Info(String.Format("$Contex.getVariable ::: ${0} is not property, u trying to get RootObj", String.Join(".", pCallList)));
                        rObject = (object)lJToken;
                        break;*/
                    case JTokenType.Boolean:
                        rObject = (Boolean)lJToken;
                        break;
                   /*case JTokenType.Bytes:
                        break;
                    case JTokenType.Comment:
                        break;
                    case JTokenType.Constructor:
                        break;
                    case JTokenType.Date:
                        break;*/
                    case JTokenType.Float:
                        rObject = (float)lJToken;
                        break;
                    /*case JTokenType.Guid:
                        break;*/
                    case JTokenType.Integer:
                        rObject = (int)lJToken;
                        break;
                    /*case JTokenType.None:
                        break;
                    case JTokenType.Null:
                        break;
                    case JTokenType.Object:
                        /*Log.Info(String.Format("$Contex.getVariable ::: ${0} is not property, u trying to get RootObj", String.Join(".", pCallList)));
                        rObject = (object)lJToken;
                        break;
                    /*case JTokenType.Property:
                        break;
                    case JTokenType.Raw:
                        break;*/
                    case JTokenType.String:
                        rObject = (string)lJToken;
                        break;
                    /*case JTokenType.TimeSpan:
                        break;
                    case JTokenType.Undefined:
                        break;
                    case JTokenType.Uri:
                        break;*/
                    default:
                        Log.Error(String.Format("$Contex.getVariable ::: ${0} unsupported property type {1}", String.Join(".", pCallList), typeOfVariable));
                        rObject = null;
                        break;
                }
                return rObject;               
            }
            else
            {     
                return null;
            }           
        }
        public JArray getConteiner(string[] pCallList)
        {
            JToken lJToken = getJToken(pCallList);
            if (lJToken != null)
            {
                JTokenType typeOfVariable = lJToken.Type;
                if (typeOfVariable == JTokenType.Array)
                {
                    return (JArray)lJToken;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }          
        }
        public object Clone()
        {
            return new Context(mDada.DeepClone() as JObject);
        }

        public void addVariable(string key, JToken pToken)
        {
            JToken dummy;
            if (mDada.TryGetValue(key, out dummy))
            {
                mDada.Remove(key);
            }
            mDada.Add(key, pToken);
        }
        public void removeVariable(string key)
        {
            mDada.Remove(key);
        }
        public void merge(Context a)
        {
            mDada.Merge(a.mDada);
        }
    }
}