﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine
{
    class Fragment
    {
        public enum TypeOfFragments { VARIABLE, END, IF, ELSEIF, ELSE, FOREACH, SET, BREAK, PLAINTEXT, MACRODEFINE, MACROCALL, INCLUDE, PARSE, ESCAPESEQUENCES };

        public string TextInside {get; private set;}
        public TypeOfFragments TypeOfFragment { get; private set; }
        public Fragment(string pInputText)
        {
            TextInside = pInputText;
            if (Variable.regex.IsMatch(pInputText))
            {
                TypeOfFragment = TypeOfFragments.VARIABLE;        
            }
            else if (Foreach.regex.IsMatch(pInputText))
            {
                TypeOfFragment = TypeOfFragments.FOREACH;
            }
            else if (End.regex.IsMatch(pInputText))
            {
                TypeOfFragment = TypeOfFragments.END;
            }
            else if (Break.regex.IsMatch(pInputText))
            {
                TypeOfFragment = TypeOfFragments.BREAK;
            }
            else if (If.regex.IsMatch(pInputText))
            {
                TypeOfFragment = TypeOfFragments.IF;
            }
            else if (Else.regex.IsMatch(pInputText))
            {
                TypeOfFragment = TypeOfFragments.ELSE;
            }
            else if (ElseIf.regex.IsMatch(pInputText))
            {
                TypeOfFragment = TypeOfFragments.ELSEIF;
            }
            else if (MacroDefine.regex.IsMatch(pInputText))
            {
                TypeOfFragment = TypeOfFragments.MACRODEFINE;
            }
            else if (MacroCall.regex.IsMatch(pInputText))
            {
                TypeOfFragment = TypeOfFragments.MACROCALL;
            }
            else if (Set.regex.IsMatch(pInputText))
            {
                TypeOfFragment = TypeOfFragments.SET;
            }
            else if (Include.regex.IsMatch(pInputText))
            {
                TypeOfFragment = TypeOfFragments.INCLUDE;
            }
            else if (Parse.regex.IsMatch(pInputText))
            {
                TypeOfFragment = TypeOfFragments.PARSE;
            }
            else if (EscapeSequences.regex.IsMatch(pInputText))
            {
                TypeOfFragment = TypeOfFragments.ESCAPESEQUENCES;
            }            
            else
            {
                TypeOfFragment = TypeOfFragments.PLAINTEXT; ;
            }
        }
        public override string ToString()
        {
            return TextInside;
        }
    }
}
