﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Engine
{
    class MacroTable :
        ICloneable
    {
        public Dictionary<String, MacroDefine> macros = new Dictionary<String, MacroDefine>();
        public void addMacro(string key, MacroDefine renderable)
        {
            if (macros.ContainsKey(key))
            {
                macros.Remove(key);
            }
            macros.Add(key, renderable);
        }
        public bool ContainsKey(string key)
        {
            return macros.ContainsKey(key);
        }
        public MacroDefine getMacro(string name)
        {
            return macros[name];
        }

        public object Clone()
        {
            MacroTable rMacroTable = new MacroTable();
            foreach(var macro in this.macros)
            {
                rMacroTable.macros.Add(macro.Key, macro.Value);
            }
            return rMacroTable;
        }
    }
}
