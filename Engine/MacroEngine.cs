﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace Engine
{
    class MacroEngine
    {
        public void merge(Config config)
        {
            Context preLoadedContext = new Context();
            MacroTable preLoadedMacroTable = new MacroTable();
            foreach (string preloadedFile in config.mPreloadedFiles)
            {
                string fileToRender = "";
                try
                {
                    fileToRender = File.ReadAllText(config.mRootPath + "\\" + preloadedFile);
                    Compiler compiler = new Compiler(fileToRender);
                    Renderable root = compiler.compile();
                    root.render(ref preLoadedContext, ref preLoadedMacroTable);
                }
                catch (Exception e)
                {
                    Log.Error(String.Format("Cannot open file {0}\nError ", preloadedFile) + e.Message);
                    return;
                }
            }
            foreach(var key in config.mFilesToParse.Keys)
            {
                string fileToRender = File.ReadAllText(config.mRootPath + "\\" + key);
                Compiler compiler = new Compiler(fileToRender);
                Renderable root = compiler.compile();
                Context contex = config.mFilesToParse[key];
                contex.merge(preLoadedContext);
                MacroTable macroForMerge = (MacroTable)preLoadedMacroTable.Clone();
                string result = root.render(ref contex, ref macroForMerge);
                //Console.WriteLine(result);
                //Console.WriteLine("_______________________________________________________________");
                System.IO.File.WriteAllText(config.mInputToOutput[key], result);              
            }
            Console.WriteLine("done");
        }
    }
}
