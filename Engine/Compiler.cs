﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Engine
{
    class Compiler
    {
        private string mTemplateString;
        private Regex mTemplateRegex;
        public Compiler(string pTemplateString)
        {
            mTemplateString = pTemplateString;
            string pattern = String.Format("({0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|{11}|{12})",
                EscapeSequences.pattern,
                If.pattern,
                ElseIf.pattern,
                Else.pattern,               
                Foreach.pattern,
                Set.pattern,                             
                End.pattern,
                MacroDefine.pattern,
                MacroCall.pattern,            
                Include.pattern,
                Parse.pattern,
                Break.pattern,
                Variable.pattern
                );
            mTemplateRegex = new Regex(pattern);
        }
        private List<Fragment> eachFragment()
        {
            List<Fragment> rList = new List<Fragment>();
            //Console.WriteLine(" - - - - - - - - - - - - - - -- Fragments - - - - - - - - - - - ");
            foreach (string fragment in mTemplateRegex.Split(mTemplateString))
            {
                rList.Add(new Fragment(fragment));
                //Console.WriteLine(fragment);
                //Console.WriteLine("_______________________________________________________________");
            }
            //Console.WriteLine(" - - - - - - - - - - - - - - end Fragments - - - - - - - - - - - ");
            return rList;
        }
        public Renderable compile()
        {
            Renderable root = new Root();
            Stack<Renderable> scopeStack = new Stack<Renderable>();
            scopeStack.Push(root);
            foreach (Fragment lFragment in eachFragment())
            {
                if (scopeStack.Count == 0)
                {
                    Log.Error("nesting issues with fragment \n" + lFragment.ToString());
                    //hot fix
                    scopeStack.Push(root);
                }
                Renderable parentScope = scopeStack.Peek();
                if (lFragment.TypeOfFragment == Fragment.TypeOfFragments.END)
                {
                    parentScope.exitScope();
                    scopeStack.Pop();
                    continue;
                }
                Renderable newRenderable = renderableFabric(lFragment);
                newRenderable.parent = parentScope;
                if (newRenderable != null)
                {
                    parentScope.addChild(newRenderable);
                    if (newRenderable.createsScope)
                    {
                        scopeStack.Push(newRenderable);
                        newRenderable.enterScope();
                    }
                }
            }
            return root;
        }
        private Renderable renderableFabric(Fragment pFragment)
        {
            switch (pFragment.TypeOfFragment)
            {
                case Fragment.TypeOfFragments.ESCAPESEQUENCES:
                    return new EscapeSequences(pFragment);
                case Fragment.TypeOfFragments.VARIABLE:
                    return new Variable(pFragment);
                case Fragment.TypeOfFragments.IF:
                    return new If(pFragment);
                case Fragment.TypeOfFragments.ELSEIF:
                    return new ElseIf(pFragment);
                case Fragment.TypeOfFragments.ELSE:
                    return new Else(pFragment);                
                case Fragment.TypeOfFragments.FOREACH:
                    return new Foreach(pFragment);
                case Fragment.TypeOfFragments.END:
                    return new End(pFragment);
                case Fragment.TypeOfFragments.SET:
                    return new Set(pFragment);
                case Fragment.TypeOfFragments.BREAK:
                    return new Break(pFragment);
                case Fragment.TypeOfFragments.MACRODEFINE:
                    return new MacroDefine(pFragment);
                case Fragment.TypeOfFragments.MACROCALL:
                    return new MacroCall(pFragment);
                case Fragment.TypeOfFragments.INCLUDE:
                    return new Include(pFragment);
                case Fragment.TypeOfFragments.PARSE:
                    return new Parse(pFragment);
                default:
                    return new PlainText(pFragment);
            }
        }
    }
}
